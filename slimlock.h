/*
 *  This file is part of schlimm
 *  Copyright (C) 2019-2021  Moritz Strohm <ncc1988@posteo.de>
 *  and others (see the AUTHORS file).
 *
 *  slimlock
 *  Copyright (c) 2010-2012 Joel Burget <joelburget@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 */


#include <cstdio>
#include <cstring>
#include <algorithm>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <linux/vt.h>
#include <X11/keysym.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/extensions/dpms.h>
#include <security/pam_appl.h>
#include <pthread.h>
#include <err.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <errno.h>
#include <sys/file.h>
#include <fcntl.h>

#include <fmt/core.h>

#include "Config.h"
#include "Log.h"
#include "Util.h"
#include "Panel.h"




void setBackground(const std::string& themedir);


void HideCursor();


bool AuthenticateUser();


static int ConvCallback(
    int num_msgs,
    const struct pam_message **msg,
    struct pam_response **resp,
    void *appdata_ptr
    );


std::string findValidRandomTheme(const std::string& set);


void HandleSignal(int sig);


void *RaiseWindow(void *data);
