/*
 *  This file is part of schlimm
 *  Copyright (C) 2019-2021  Moritz Strohm <ncc1988@posteo.de>
 *  and others (see the AUTHORS file).
 *
 *  SLiM - Simple Login Manager
 *  Copyright (C) 1997, 1998 Per Liden
 *  Copyright (C) 2004-06 Simone Rota <sip@varlock.com>
 *  Copyright (C) 2004-06 Johannes Winkelmann <jw@tks6.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 */


#ifndef _SWITCHUSER_H_
#define _SWITCHUSER_H_


#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pwd.h>
#include <grp.h>
#include <paths.h>
#include <cstdio>
#include <iostream>


#include "Log.h"
#include "Config.h"
#include "Util.h"


class SwitchUser
{
    public:


    SwitchUser(
        struct passwd* pw,
        std::shared_ptr<Schlimm::Config> c,
        const std::string& display,
        std::vector<std::string>& env
        );


    ~SwitchUser();


    void Login(const char* cmd, const char* mcookie);


    protected:


    //SwitchUser();


    void SetEnvironment();


    void SetUserId();


    void Execute(const char* cmd);


    void SetClientAuth(const char* mcookie);


    std::shared_ptr<Schlimm::Config> cfg;


    struct passwd *Pw;


    std::string displayName;


    std::vector<std::string> env;
};


#endif
