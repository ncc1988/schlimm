/*
 *  This file is part of schlimm
 *  Copyright (C) 2019-2021  Moritz Strohm <ncc1988@posteo.de>
 *  and others (see the AUTHORS file).
 *
 *  SLiM - Simple Login Manager
 *  Copyright (C) 1997, 1998 Per Liden
 *  Copyright (C) 2004-06 Simone Rota <sip@varlock.com>
 *  Copyright (C) 2004-06 Johannes Winkelmann <jw@tks6.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 */


#include "Log.h"


using namespace Schlimm;


std::shared_ptr<Log> Log::instance = nullptr;


Log::Log(
    SingletonEnforcer s,
    const std::string& app_name,
    const std::string& log_file_name
    )
    : app_name(app_name)
{
    if (!log_file_name.empty()) {
        this->log_file.open(log_file_name, std::ios_base::app);
        if (!this->log_file.is_open()) {
            this->log(
                fmt::format(
                    "Could not accesss log file \"{}\"!",
                    log_file_name)
                );
        }
    }
}


Log::~Log()
{
    if (this->log_file.is_open()) {
        this->log_file.close();
    }
}


std::shared_ptr<Log> Log::init(
    const std::string& app_name,
    const std::string& log_file_name
    )
{
    if (Log::instance == nullptr) {
        Log::instance = std::make_shared<Log>(SingletonEnforcer{}, app_name, log_file_name);
    }
    return Log::instance;
}


std::shared_ptr<Log> Log::getInstance()
{
    return Log::instance;
}


void Log::append(const std::string& text)
{
    std::string real_text = "";
    if (!this->app_name.empty()) {
        real_text = this->app_name + ": " + text;
    } else {
        real_text = text;
    }
    if (this->log_file.is_open()) {
        this->log_file << real_text << std::endl;
        this->log_file.flush();
    }
    std::cerr << real_text << std::endl;
    std::cerr.flush();
}


void Log::log(const std::string& text)
{
    auto log = Log::init();
    if (log != nullptr) {
        log->append(text);
    }
}
