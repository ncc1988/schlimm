/*
 *  This file is part of schlimm
 *  Copyright (C) 2019-2020  Moritz Strohm <ncc1988@posteo.de>
 *  and others (see the AUTHORS file).
 *
 *  SLiM - Simple Login Manager
 *  Copyright (C) 2004-06 Simone Rota <sip@varlock.com>
 *  Copyright (C) 2004-06 Johannes Winkelmann <jw@tks6.net>
 *  Copyright (C) 2012-13 Nobuhiro Iwamatsu <iwamatsu@nigauri.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 */


#include "Config.h"


using namespace Schlimm;



Config::Config(const std::string& config_file)
    : currentSession(-1)
{
    //Try to read the configuration file first. If that fails,
    //load default values as best guess.
    if (!this->readConf(config_file)) {
        Log::log("Loading default settings!");
        /* Configuration options */
        options.insert(option("default_path","/bin:/usr/bin:/usr/local/bin"));
        options.insert(option("default_xserver","/usr/bin/X"));
        options.insert(option("xserver_arguments",""));
        options.insert(option("numlock",""));
        options.insert(option("daemon",""));
        options.insert(option("xauth_path","/usr/bin/xauth"));
        //Check if we can use Xsession as login command:
        try {
            if (std::filesystem::exists("/etc/X11/Xsession")) {
                //Use Xsession as login command.
                options.insert(option("login_cmd", "exec /etc/X11/Xsession %session"));
            } else {
                //Use the default from SLiM as last resort.
                options.insert(option("login_cmd", "exec /bin/bash -login ~/.xinitrc %session"));
            }
        } catch (std::filesystem::filesystem_error& error) {
            Log::log(
                fmt::format(
                    "File system error occurred when trying to check the existence of /etc/X11/Xsession: \"{}\". Using ~/.xinitrc as fallback login command!",
                    error.what()
                    )
                );
            options.insert(option("login_cmd", "exec /bin/bash -login ~/.xinitrc %session"));
        }

        options.insert(option("halt_cmd","/sbin/shutdown -h now"));
        options.insert(option("reboot_cmd","/sbin/shutdown -r now"));
        options.insert(option("suspend_cmd",""));
        options.insert(option("sessionstart_cmd",""));
        options.insert(option("sessionstop_cmd",""));
        options.insert(option("console_cmd","/usr/bin/xterm -C -fg white -bg black +sb -g %dx%d+%d+%d -fn %dx%d -T ""Console login"" -e /bin/sh -c ""/bin/cat /etc/issue; exec /bin/login"""));
        options.insert(option("screenshot_cmd","import -window root /slim.png"));
        options.insert(option("welcome_msg","Welcome to %host"));
        options.insert(option("session_msg","Session:"));
        options.insert(option("default_user",""));
        options.insert(option("focus_password","no"));
        options.insert(option("auto_login","no"));
        options.insert(option("current_theme","default"));
        options.insert(option("lockfile","/var/run/slim.lock"));
        options.insert(option("logfile","/var/log/slim.log"));
        options.insert(option("authfile","/var/run/slim.auth"));
        options.insert(option("shutdown_msg","The system is halting..."));
        options.insert(option("reboot_msg","The system is rebooting..."));
        options.insert(option("sessiondir",""));
        //options.insert(option("sessiondir","/usr/share/xsessions"));
        options.insert(option("hidecursor","false"));

        /* Theme stuff */
        options.insert(option("input_panel_x","50%"));
        options.insert(option("input_panel_y","40%"));
        options.insert(option("input_name_x","200"));
        options.insert(option("input_name_y","154"));
        options.insert(option("input_pass_x","-1")); /* default is single inputbox */
        options.insert(option("input_pass_y","-1"));
        options.insert(option("input_font","Verdana:size=11"));
        options.insert(option("input_color", "#FFFFFF"));
        options.insert(option("input_cursor_height","20"));
        options.insert(option("input_maxlength_name","20"));
        options.insert(option("input_maxlength_passwd","20"));
        options.insert(option("input_shadow_xoffset", "0"));
        options.insert(option("input_shadow_yoffset", "0"));
        options.insert(option("input_shadow_color","#FFFFFF"));

        options.insert(option("welcome_font","Verdana:size=14"));
        options.insert(option("welcome_color","#FFFFFF"));
        options.insert(option("welcome_x","-1"));
        options.insert(option("welcome_y","-1"));
        options.insert(option("welcome_shadow_xoffset", "0"));
        options.insert(option("welcome_shadow_yoffset", "0"));
        options.insert(option("welcome_shadow_color","#FFFFFF"));

        options.insert(option("intro_msg",""));
        options.insert(option("intro_font","Verdana:size=14"));
        options.insert(option("intro_color","#FFFFFF"));
        options.insert(option("intro_x","-1"));
        options.insert(option("intro_y","-1"));

        options.insert(option("background_style","color"));
        options.insert(option("background_color","#CCCCCC"));

        options.insert(option("username_font","Verdana:size=12"));
        options.insert(option("username_color","#FFFFFF"));
        options.insert(option("username_x","-1"));
        options.insert(option("username_y","-1"));
        options.insert(option("username_msg","Please enter your username"));
        options.insert(option("username_shadow_xoffset", "0"));
        options.insert(option("username_shadow_yoffset", "0"));
        options.insert(option("username_shadow_color","#FFFFFF"));

        options.insert(option("password_x","-1"));
        options.insert(option("password_y","-1"));
        options.insert(option("password_msg","Please enter your password"));

        options.insert(option("msg_color","#FFFFFF"));
        options.insert(option("msg_font","Verdana:size=16:bold"));
        options.insert(option("msg_x","40"));
        options.insert(option("msg_y","40"));
        options.insert(option("msg_shadow_xoffset", "0"));
        options.insert(option("msg_shadow_yoffset", "0"));
        options.insert(option("msg_shadow_color","#FFFFFF"));

        options.insert(option("session_color","#FFFFFF"));
        options.insert(option("session_font","Verdana:size=16:bold"));
        options.insert(option("session_x","50%"));
        options.insert(option("session_y","90%"));
        options.insert(option("session_shadow_xoffset", "0"));
        options.insert(option("session_shadow_yoffset", "0"));
        options.insert(option("session_shadow_color","#FFFFFF"));

        // slimlock-specific options
        options.insert(option("dpms_standby_timeout", "60"));
        options.insert(option("dpms_off_timeout", "600"));
        options.insert(option("wrong_passwd_timeout", "2"));
        options.insert(option("passwd_feedback_x", "50%"));
        options.insert(option("passwd_feedback_y", "10%"));
        options.insert(option("passwd_feedback_msg", "Authentication failed"));
        options.insert(option("passwd_feedback_capslock", "Authentication failed (CapsLock is on)"));
        options.insert(option("show_username", "1"));
        options.insert(option("show_welcome_msg", "0"));
        options.insert(option("tty_lock", "1"));
        options.insert(option("bell", "1"));
    }
}


Config::~Config()
{
    options.clear();
}


bool Config::readConf(std::string configfile)
{
    int n = -1;
    size_t pos = 0;
    std::string line, next, op, fn(configfile);
    std::map<std::string, std::string>::iterator it;
    std::ifstream cfgfile(fn.c_str());

    if (!cfgfile) {
        Log::log(
            fmt::format(
                "Cannot read configuration file \"{}\"!",
                configfile
                )
            );
        return false;
    }
    while (std::getline(cfgfile, line)) {
        if ((pos = line.find('\\')) != std::string::npos) {
            if (line.length() == pos + 1) {
                line.replace(pos, 1, " ");
                next = next + line;
                continue;
            } else {
                line.replace(pos, line.length() - pos, " ");
            }
        }

        if (!next.empty()) {
            line = next + line;
            next = "";
        }
        it = options.begin();
        while (it != options.end()) {
            op = it->first;
            n = line.find(op);
            if (n == 0) {
                options[op] = parseOption(line, op);
            }
            ++it;
        }
    }
    cfgfile.close();

    this->fillSessionList();

    return true;
}



std::string Config::parseOption(std::string line, std::string option)
{
    return Trim(line.substr(option.size(), line.size() - option.size()));
}


std::string& Config::getOption(std::string option)
{
    return options[option];
}



std::string Config::Trim(const std::string& s)
{
    if (s.empty()) {
        return s;
    }
    int pos = 0;
    std::string line = s;
    int len = line.length();
    while ((pos < len) && isspace(line[pos])) {
        ++pos;
    }
    line.erase(0, pos);
    pos = line.length() - 1;
    while ((pos > -1) && isspace(line[pos])) {
        --pos;
    }
    if (pos != -1) {
        line.erase(pos + 1);
    }
    return line;
}


std::string Config::getWelcomeMessage()
{
    std::string s = getOption("welcome_msg");
    int n = s.find("%host");
    if (n >= 0) {
        std::string tmp = s.substr(0, n);
        char host[40];
        gethostname(host,40);
        tmp = tmp + host;
        tmp = tmp + s.substr(n+5, s.size() - n);
        s = tmp;
    }
    n = s.find("%domain");
    if (n >= 0) {
        std::string tmp = s.substr(0, n);;
        char domain[40];
        getdomainname(domain,40);
        tmp = tmp + domain;
        tmp = tmp + s.substr(n+7, s.size() - n);
        s = tmp;
    }
    return s;
}


int Config::string2int(const char* string, bool* ok)
{
    char* err = 0;
    int l = (int)strtol(string, &err, 10);
    if (ok) {
        *ok = (*err == 0);
    }
    return (*err == 0) ? l : 0;
}


int Config::getIntOption(std::string option)
{
    return string2int(options[option].c_str());
}



int Config::absolutepos(const std::string& position, int max, int width)
{
    int n = position.find("%");
    if (n>0) { /* X Position expressed in percentage */
        int result = (max*string2int(position.substr(0, n).c_str())/100) - (width / 2);
        return result < 0 ? 0 : result ;
    } else { /* Absolute X position */
        return string2int(position.c_str());
    }
}


void Config::split(
    std::vector<std::string>& v,
    const std::string& str,
    char c,
    bool useEmpty
    )
{
    v.clear();
    std::string::const_iterator s = str.begin();
    std::string tmp;
    while (true) {
        std::string::const_iterator begin = s;
        while (*s != c && s != str.end()) {
            ++s;
        }
        tmp = std::string(begin, s);
        if (useEmpty || tmp.size() > 0) {
            v.push_back(tmp);
        }
        if (s == str.end()) {
            break;
        }
        if (++s == str.end()) {
            if (useEmpty) {
                v.push_back("");
            }
            break;
        }
    }
}


void Config::fillSessionList()
{
    std::string strSessionDir = getOption("sessiondir");
    if (strSessionDir.empty()) {
        //We cannot load any session.
        Log::log("No session directory is specified in the configuration! Cannot load sessions!");
        return;
    }

    sessions.clear();

    if( !strSessionDir.empty() ) {
        DIR *pDir = opendir(strSessionDir.c_str());

        if (pDir != NULL) {
            struct dirent *pDirent = NULL;

            while ((pDirent = readdir(pDir)) != NULL) {
                std::string strFile(strSessionDir);
                strFile += "/";
                strFile += pDirent->d_name;

                struct stat oFileStat;

                if (stat(strFile.c_str(), &oFileStat) == 0) {
                    if (S_ISREG(oFileStat.st_mode) &&
                            access(strFile.c_str(), R_OK) == 0) {
                        std::ifstream desktop_file(strFile.c_str());
                        if (desktop_file) {
                            std::string line, session_name = "", session_exec = "";
                            while (getline( desktop_file, line )) {
                                if (line.substr(0, 5) == "Name=") {
                                    session_name = line.substr(5);
                                    if (!session_exec.empty()) {
                                         break;
                                    }
                                } else {
                                    if (line.substr(0, 5) == "Exec=") {
                                        session_exec = line.substr(5);
                                        if (!session_name.empty()) {
                                            break;
                                        }
                                    }
                                }
                            }
                            desktop_file.close();
                            std::pair<std::string, std::string> session(
                                session_name,
                                session_exec
                                );
                            sessions.push_back(session);
                            std::cout << session_exec << " - " << session_name << std::endl;
                        }
                    }
                }
            }
            closedir(pDir);
        }
    }

    if (sessions.empty()) {
        std::pair<std::string, std::string> session("","");
        sessions.push_back(session);
    }
}


bool Config::hasSessions() const
{
    return !this->sessions.empty();
}

std::pair<std::string, std::string> Config::nextSession()
{
    if (sessions.size() > 0) {
        currentSession = (currentSession + 1) % sessions.size();
    }
    return sessions[currentSession];
}
