/* SLiM - Simple Login Manager
   Copyright (C) 1997, 1998 Per Liden
   Copyright (C) 2004-06 Simone Rota <sip@varlock.com>
   Copyright (C) 2004-06 Johannes Winkelmann <jw@tks6.net>
   Copyright (C) 2013 Nobuhiro Iwamatsu <iwamatsu@nigauri.org>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
*/

#ifndef _PANEL_H_
#define _PANEL_H_

#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <X11/Xft/Xft.h>
#include <X11/cursorfont.h>
#include <X11/Xmu/WinUtil.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <signal.h>
#include <iostream>
#include <string>
#include <sstream>
#include <poll.h>
#include <X11/extensions/Xrandr.h>


#ifdef NEEDS_BASENAME
#include <libgen.h>
#endif


#include "SwitchUser.h"
#include "Log.h"
#include "Image.h"


namespace Schlimm
{
    struct Rectangle
    {
        int x = 0;


        int y = 0;


        unsigned int width = 0;


        unsigned int height = 0;


        Rectangle()
        {
        };


        Rectangle(
            int x,
            int y,
            unsigned int width,
            unsigned int height)
            : x(x),
              y(y),
              width(width),
              height(height)
        {
        };


        bool is_empty() const
        {
            return this->width == 0 || this->height == 0;
        }
    };


    class Panel
    {
        public:


        enum ActionType
        {
            Login,
            Lock,
            Console,
            Reboot,
            Halt,
            Exit,
            Suspend
        };


        enum FieldType
        {
            Get_Name,
            Get_Passwd
        };


        enum PanelType
        {
            Mode_DM,
            Mode_Lock
        };


        Panel(
            Display *dpy,
            int scr,
            Window root,
            Schlimm::Config* config,
            const std::string& themed,
            PanelType panel_mode
            );


        ~Panel();


        void openPanel();


        void closePanel();


        void clearPanel();


        void wrongPassword(int timeout);


        void message(const std::string &text);


        void error(const std::string &text);


        void eventHandler(const FieldType &curfield);


        std::string getSession();


        ActionType getAction(void) const;


        void reset(void);


        void resetName(void);


        void resetPasswd(void);


        void setName(const std::string& name);


        const std::string& getName(void) const;


        const std::string& getPasswd(void) const;


        void switchSession();


        protected:


        Panel();


        void cursor(int visible);


        unsigned long getColor(const char *colorname);


        void onExpose(void);


        void eraseLastChar(std::string& formerString);


        bool onKeyPress(XEvent& event);


        void showText();


        void showSession();


        void slimDrawString8(
            XftDraw *d,
            XftColor *color,
            XftFont *font,
            int x,
            int y,
            const std::string &str,
            XftColor* shadowColor,
            int xOffset,
            int yOffset
            );


        Rectangle getPrimaryViewport();


        void applyBackground(Rectangle = Rectangle());


        PanelType mode; /* work mode */


        Schlimm::Config* config;


        Window window;


        Window root_window;


        Display* display;


        int screen;


        int X, Y;


        GC text_gc;


        GC win_gc;


        XftFont* font;


        XftColor inputshadowcolor;


        XftColor inputcolor;


        XftColor msgcolor;


        XftColor msgshadowcolor;


        XftFont* msgfont;


        XftColor introcolor;


        XftFont* introfont;


        XftFont* welcomefont;


        XftColor welcomecolor;


        XftFont* sessionfont;


        XftColor sessioncolor;


        XftColor sessionshadowcolor;


        XftColor welcomeshadowcolor;


        XftFont* enterfont;


        XftColor entercolor;


        XftColor entershadowcolor;


        ActionType action;


        FieldType field;


        /* Username/Password */


        std::string NameBuffer = "";


        std::string PasswdBuffer = "";


        std::string HiddenPasswdBuffer = "";


        /* screen stuff */


        Rectangle viewport;


        /* Configuration */


        int input_name_x;


        int input_name_y;


        int input_pass_x;


        int input_pass_y;


        int inputShadowXOffset;


        int inputShadowYOffset;


        int input_cursor_height;


        int welcome_x;


        int welcome_y;


        int welcome_shadow_xoffset;


        int welcome_shadow_yoffset;


        int session_shadow_xoffset;


        int session_shadow_yoffset;


        int intro_x;


        int intro_y;


        int username_x;


        int username_y;


        int username_shadow_xoffset;


        int username_shadow_yoffset;


        int password_x;


        int password_y;


        std::string welcome_message;


        std::string intro_message;


        /* Pixmap data */


        Pixmap panel_pixmap;


        Schlimm::Image* image;


        /* For testing themes */


        bool testing;


        std::string themedir;


        /* Session handling */


        std::string session_name;


        std::string session_exec;
    };
}


#endif /* _PANEL_H_ */
