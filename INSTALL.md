# Installation procedure for Schlimm

## Prerequesites

You need the development files for the following libraries:

- jpeg
- pam
- x11
- xft
- xmu
- xrandr

Furthermore, the following programs must be installed:

- cmake
- fontconfig
- pkg-config

On Devuan GNU+Linux and related operating systems (like Debian GNU/Linux
or Ubuntu Linux) you can use the following command to install the relevant
packages as root user:

    apt-get install cmake fontconfig libjpeg-dev libpam-dev libx11-dev libxft-dev libxmu-dev libxrandr-dev


After that, checkout the source code with git:

    git clone https://codeberg.org/ncc1988/schlimm.git

You will now have a subfolder named "schlimm" in the current directory.
Enter that directory and create a subdirectory named "build". This is used
for all compiled files. Using the "build" subdirectory prevents your source
code copy to be messed up with compiled files.
After you entered the "build" subdirectory, run CMake to configure your build.
At the moment, building Schlimm with PAM support is known to work, so that CMake
can be invoked like this:

    cmake -DUSE_PAM=1 ..

After CMake finished building makefiles, you can run make to compile Schlimm.
You can run make with the -jX argument where X is the number of jobs to be run
simultaneously. This speeds up things on multicore systems.

When make finished the compilation process without errors, you have
successfully built Schlimm yourself. :)




--------
Original SLiM installation procedure below:
--------

INSTALL file for SLiM

0. Prerequisites:
 - cmake
 - X.org or XFree86
 - libxmu
 - libpng
 - libjpeg

1. to build and install the program:
 - edit the Makefile to adjust libraries and paths to your OS (if needed)
 - mkdir build ; cd build ; cmake ..
     or
 - mkdir build ; cd build ; cmake .. -DUSE_PAM=yes to enable PAM support
     or
 - mkdir build ; cd build ; cmake .. -DUSE_CONSOLEKIT=yes
   to enable CONSOLEKIT support
 - make && make install
 
2. automatic startup
Edit the init scripts according to your OS/Distribution.
