/* 
 *  This file is part of schlimm
 *  Copyright (C) 2019-2020  Moritz Strohm <ncc1988@posteo.de>
 *  and others (see the AUTHORS file).
 *
 * SLiM - Simple Login Manager
 * Copyright (C) 2004-06 Simone Rota <sip@varlock.com>
 * Copyright (C) 2004-06 Johannes Winkelmann <jw@tks6.net>
 * Copyright (C) 2012	Nobuhiro Iwamatsu <iwamatsu@nigauri.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The following code has been adapted and extended from
 * xplanet 1.0.1, Copyright (C) 2002-04 Hari Nair <hari@alumni.caltech.edu>
 */


#ifndef SCHLIMM_IMAGE_H
#define SCHLIMM_IMAGE_H


#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <X11/Xlib.h>
#include <X11/Xmu/WinUtil.h>


#include "Log.h"




namespace Schlimm
{
    class Image
    {
        public:


        Image();


        Image(
            const uint32_t width,
            const uint32_t height,
            const unsigned char* rgb,
            const unsigned char* alpha
            );


        ~Image();


        /**
         * @returns True if an image file is loaded, false otherwise.
         */
        bool imageLoaded() const;


        const unsigned char *getPNGAlpha() const
        {
            return(png_alpha);
        };


        const unsigned char *getRGBData() const
        {
            return(rgb_data);
        };


        [[deprecated]] void getPixel(
            double px,
            double py,
            unsigned char *pixel
            );


        /**
         * Find the color of the desired point using bilinear interpolation.
         * Assume the array indices refer to the denter of the pixel, so each
         * pixel has corners at (i - 0.5, j - 0.5) and (i + 0.5, j + 0.5)
         */
        void getPixel(
            double px,
            double py,
            unsigned char *pixel,
            unsigned char *alpha
            );


        int Width() const
        {
            return(width);
        };


        int Height() const
        {
            return(height);
        };


        void Quality(const int q)
        {
            quality_ = q;
        };


        bool Read(const char *filename);


        void Reduce(const int factor);


        void Resize(const int w, const int h);


       /**
        * Merge the image with a background, taking care of the
        * image Alpha transparency. (background alpha is ignored).
        * The images is merged on position (x, y) on the
        * background, the background must contain the image.
        */
       void Merge(Image *background, const int x, const int y);


        void Merge_non_crop(Image* background, const int x, const int y);


        void Crop(const int x, const int y, const int w, const int h);


        void Tile(const int w, const int h);


        void Center(const int w, const int h, const char *hex);


        void Plain(const int w, const int h, const char *hex);


        void computeShift(unsigned long mask, unsigned char &left_shift,


        unsigned char &right_shift);


        Pixmap createPixmap(Display *dpy, int scr, Window win);


        protected:


        bool image_loaded = false;


        int width = 0;


        int height = 0;


        int area = 0;


        unsigned char *rgb_data = nullptr;


        unsigned char *png_alpha = nullptr;


        int quality_ = 0;


        int readJpeg(
            const char *filename,
            int *width,
            int *height,
            unsigned char **rgb
            );


        int readPng(
            const char *filename,
            int *width,
            int *height,
            unsigned char **rgb,
            unsigned char **alpha
            );
    };
}


#endif
