/*
 *  This file is part of schlimm
 *  Copyright (C) 2019  Moritz Strohm <ncc1988@posteo.de>
 *  and others (see the AUTHORS file).
 *
 *  SLiM - Simple Login Manager
 *  Copyright (C) 2007 Martin Parm
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef EXCEPTION_H
#define EXCEPTION_H


#include <string>


namespace Schlimm
{
    class Exception
    {
        public:

        int error_code;

        std::string error_message;

        std::string function_name;

        Exception(
            const std::string& error_message,
            const std::string& function_name,
            int error_code = -1
            );

        std::string toString();
    };
}


#endif
