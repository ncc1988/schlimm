/*
 *  This file is part of schlimm
 *  Copyright (C) 2019-2020  Moritz Strohm <ncc1988@posteo.de>
 *  and others (see the AUTHORS file).
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include "X11XLibClient.h"


using namespace Schlimm;


X11XLibClient::X11XLibClient(const std::string& display_name)
    : display_name(display_name)
{
    this->log = Log::getInstance();
}


std::string X11XLibClient::getDisplayName()
{
    return this->display_name;
}


bool X11XLibClient::init(const std::string& xauth_command, const std::string& authority_file)
{
    // We rely on the fact that all bits generated by Util::random()
    // are usable, so we are taking full words from its output.
    uint16_t word;
    uint8_t hi, lo;
    int i;
    std::string authfile;
    const char *digits = "0123456789abcdef";
    Util::srandom(Util::makeseed());
    for (i = 0; i < X11XLibClient::mcookiesize; i+=4) {
        word = Util::random() & 0xffff;
        lo = word & 0xff;
        hi = word >> 8;
        this->mcookie[i] = digits[lo & 0x0f];
        this->mcookie[i+1] = digits[lo >> 4];
        this->mcookie[i+2] = digits[hi & 0x0f];
        this->mcookie[i+3] = digits[hi >> 4];
    }
    //Re-initialise the auth file:
    remove(authority_file.c_str());
    setenv("XAUTHORITY", authority_file.c_str(), 1);

    //Tell the mcookie to the X authority program:
    FILE *fp;
    std::string cmd = xauth_command + " -f " + authfile + " -q";

    fp = popen(cmd.c_str(), "w");
    if (!fp) {
        return false;
    }
    fprintf(fp, "remove %s\n", ":0");
    fprintf(fp, "add %s %s %s\n", ":0", ".", this->mcookie.c_str());
    fprintf(fp, "exit\n");
    pclose(fp);

    this->display = XOpenDisplay(this->display_name.c_str());
    if (this->display == nullptr) {
        return false;
    }

    //Set the screen and the root window:
    this->screen = DefaultScreen(this->display);
    this->root_window = RootWindow(this->display, this->screen);

    return true;
}


void X11XLibClient::blankScreen()
{
    GC gc = XCreateGC(this->display, this->root_window, 0, 0);
    XSetForeground(this->display, gc, BlackPixel(this->display, this->screen));
    XFillRectangle(
        this->display,
        this->root_window,
        gc,
        0,
        0,
        XWidthOfScreen(ScreenOfDisplay(this->display, this->screen)),
        XHeightOfScreen(ScreenOfDisplay(this->display, this->screen))
        );
    XFlush(this->display);
    XFreeGC(this->display, gc);
}
