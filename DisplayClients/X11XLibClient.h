/*
 *  This file is part of schlimm
 *  Copyright (C) 2019-2020  Moritz Strohm <ncc1988@posteo.de>
 *  and others (see the AUTHORS file).
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef SCHLIMM_X11XLIBCLIENT
#define SCHLIMM_X11XLIBCLIENT


#include <string>


#include <X11/Xlib.h>


#include "../Log.h"
#include "../Util.h"


namespace Schlimm
{
    /**
     * This class contains code to interact with an X11 server using
     * the XLib library.
     */
    class X11XLibClient
    {
        public:


        X11XLibClient(const std::string& display_name = "");


        std::string getDisplayName();


        /**
         * Initialises access to the display referenced by the
         * display_name attribute and sets up the root window.
         *
         * @returns bool True, if the initialisation has been successful,
         *     false otherwise.
         */
        bool init(const std::string& xauth_command, const std::string& authority_file);


        /**
         * Blanks the screen by filling it with black pixels.
         */
        void blankScreen();


        /**
         * DO NOT USE. This method is only a sort of "compatibility layer" for
         * old code in the App class that hasn't been moved yet!
         */
        [[deprecated]] int getScreen() {
            return this->screen;
        }


        /**
         * DO NOT USE. This method is only a sort of "compatibility layer" for
         * old code in the App class that hasn't been moved yet!
         */
        [[deprecated]] Window getRootWindow() {
            return this->root_window;
        }


        protected:


        /**
         * The instance of the Log class that is used
         * for logging messages.
         */
        std::shared_ptr<Log> log = nullptr;


        /**
         * The X11 display name to be used.
         */
        std::string display_name = "";


        /**
         * The XLib display handle.
         */
        Display* display;


        /**
         * The X11 screen to be used.
         */
        int screen = 0;


        /**
         * The width of the screen in pixels.
         */
        int screen_width = 0;


        /**
         * The height of the screen in pixels.
         */
        int screen_height = 0;


        /**
         * The X11 root window.
         */
        Window root_window;


        std::string authority_file = "";


        std::string mcookie;


        const int mcookiesize = 32;  //Must be divisible by 4
    };
}


#endif
