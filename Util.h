/* SLiM - Simple Login Manager
   Copyright (C) 2009 Eygene Ryabinkin <rea@codelabs.ru>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
*/


#ifndef _UTIL_H__
#define _UTIL_H__


#include <string>
#include <sys/types.h>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>


namespace Util
{
    /**
     * Adds the given cookie to the specified Xauthority file.
     * Returns true on success, false on fault.
     */
    [[deprecated]] bool add_mcookie(
        const std::string &mcookie,
        const char *display,
        const std::string &xauth_cmd,
        const std::string &authfile
        );


    /**
     * Interface for random number generator.  Just now it uses ordinary
     * random/srandom routines and serves as a wrapper for them.
     *
     * TODO: check why this wrapper is needed
     */
    void srandom(unsigned long seed);


    long random(void);


    /**
     * Makes seed for the srandom() using "random" values obtained from
     * getpid(), time(NULL) and others.
     */
    long makeseed(void);
}


#endif
