/*
 *  This file is part of schlimm
 *  Copyright (C) 2019-2020  Moritz Strohm <ncc1988@posteo.de>
 *  and others (see the AUTHORS file).
 *
 * SLiM - Simple Login Manager
 * Copyright (C) 2004-06 Simone Rota <sip@varlock.com>
 * Copyright (C) 2004-06 Johannes Winkelmann <jw@tks6.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef SCHLIMM_CONFIG_H
#define SCHLIMM_CONFIG_H


#include <string>
#include <map>
#include <vector>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <unistd.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

#include <fmt/core.h>

#include "Log.h"


constexpr int INPUT_MAXLENGTH_NAME = 30;
constexpr int INPUT_MAXLENGTH_PASSWD = 50;

#define CFGFILE SYSCONFDIR"/slim.conf"
#define THEMESDIR PKGDATADIR"/themes"
#define THEMESFILE "/slim.theme"




namespace Schlimm
{
    class Config
    {
        public:


        Config(const std::string& config_file = "/etc/schlimm.conf");


        ~Config();


        /**
         * Creates a Configuration object and parses known options from the
         * specified configuration file or theme file.
         */
        bool readConf(std::string configfile);


        /**
         * Returns the trimmed option value.
         */
        std::string parseOption(std::string line, std::string option);


        const std::string& getError() const;


        std::string& getOption(std::string option);


        int getIntOption(std::string option);


        /**
         * Returns the welcome message with replaced variables.
         */
        std::string getWelcomeMessage();


        /*
         * Get an absolute pixel position from a value that can be
         * a percentage.
         */
        static int absolutepos(const std::string& position, int max, int width);


        static int string2int(const char* string, bool *ok = 0);


        /*
         * Splits a comma separated string into a vector of strings.
         */
        static void split(
            std::vector<std::string> &v,
            const std::string &str,
            char c,
            bool useEmpty = true
            );


        /**
         * Returns a trimmed string.
         */
        static std::string Trim(const std::string &s);


        /**
         * @returns True, when sessions are available, false otherwise.
         */
        bool hasSessions() const;


        std::pair<std::string,std::string> nextSession();


        protected:


        void fillSessionList();


        std::map<std::string,std::string> options;


        std::vector<std::pair<std::string,std::string> > sessions;


        int currentSession;
    };


    typedef std::pair<std::string, std::string> option;
}


#endif
