/* SLiM - Simple Login Manager
   Copyright (C) 1997, 1998 Per Liden
   Copyright (C) 2004-06 Simone Rota <sip@varlock.com>
   Copyright (C) 2004-06 Johannes Winkelmann <jw@tks6.net>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
*/


#include <memory>

#include "App.h"


int main(int argc, char** argv)
{
    App::init(argc, argv);
    std::shared_ptr<App> login_app = App::getInstance();
    login_app->run();
    return 0;
}
