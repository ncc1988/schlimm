/*
 *  This file is part of schlimm
 *  Copyright (C) 2020  Moritz Strohm <ncc1988@posteo.de>
 *  and others (see the AUTHORS file).
 *
 *  SLiM - Simple Login Manager
 *  Copyright (C) 2004-06 Simone Rota <sip@varlock.com>
 *  Copyright (C) 2004-06 Johannes Winkelmann <jw@tks6.net>
 *  Copyright (C) 2012    Nobuhiro Iwamatsu <iwamatsu@nigauri.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef NUMLOCK_H
#define NUMLOCK_H


#include <X11/Xlib.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>


namespace Schlimm
{
    class NumLock
    {
        public:


        NumLock();


        static void setOn(Display *dpy);


        static void setOff(Display *dpy);


        protected:


        static int xkb_init(Display *dpy);


        static unsigned int xkb_mask_modifier(XkbDescPtr xkb, const char *name);


        static unsigned int xkb_numlock_mask(Display *dpy);


        static void control_numlock(Display *dpy, bool flag);
    };
}


#endif
