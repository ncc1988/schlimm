/*
 *  This file is part of schlimm
 *  Copyright (C) 2019  Moritz Strohm <ncc1988@posteo.de>
 *  and others (see the AUTHORS file).
 *
 *  SLiM - Simple Login Manager
 *  Copyright (C) 2007 Martin Parm
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef _PAM_H_
#define _PAM_H_


#include <iostream>
#include <security/pam_appl.h>
#include <string>

#ifdef __LIBPAM_VERSION
#include <security/pam_misc.h>
#endif


#include "Authenticator.h"
#include "Exception.h"
#include "Panel.h"
#include "Log.h"


namespace PAM
{
    class Auth_Exception: public Schlimm::Exception
    {
        public:

        Auth_Exception(
            pam_handle_t* _pam_handle,
            const std::string& _func_name,
            int _errnum
            );
    };


    class Cred_Exception: public Schlimm::Exception
    {
        public:

        Cred_Exception(
            pam_handle_t* _pam_handle,
            const std::string& _func_name,
            int _errnum
            );
    };


    //To be moved inside the PAMAuth class
    int conv(int num_msg, const struct pam_message **msg,
             struct pam_response **resp, void *appdata_ptr);


    class PAMAuth: Schlimm::Authenticator
    {
        private:

        struct pam_conv pam_conversation;

        pam_handle_t* pam_handle;

        int last_result;

        int _end(void);


        public:


        typedef int (conversation)(
            int num_msg,
            const struct pam_message **msg,
            struct pam_response **resp,
            void *appdata_ptr
            );

        enum ItemType
        {
            Service     = PAM_SERVICE,
            User        = PAM_USER,
            User_Prompt = PAM_USER_PROMPT,
            TTY         = PAM_TTY,
            Requestor   = PAM_RUSER,
            Host        = PAM_RHOST,
            Conv        = PAM_CONV,
#ifdef __LIBPAM_VERSION
            /* Fail_Delay  = PAM_FAIL_DELAY */
#endif
        };


        protected:


        /**
         * A pointer to the panel where the login GUI elements
         * are displayed.
         */
        Schlimm::Panel* panel;


        public:


        PAMAuth();


        [[deprecated]] PAMAuth(conversation* conv, void* data=0);


        ~PAMAuth(void);


        void setup();


        void setPanel(Schlimm::Panel* panel);


        void start(const std::string& service);

        void end(void);

        void set_item(const ItemType item, const void* value);

        const void* get_item(const ItemType item);

#ifdef __LIBPAM_VERSION
        void fail_delay(const unsigned int micro_sec);
#endif

        void authenticate(void);

        void openSession(void);

        void closeSession(void);

        void setenv(const std::string& key, const std::string& value);

        void delenv(const std::string& key);

        const char* getenv(const std::string& key);

        std::vector<std::string> getenvlist(void);


        /* Explicitly disable copy constructor and copy assignment */

        PAMAuth(const PAM::PAMAuth&) = delete;

        PAMAuth& operator=(const PAM::PAMAuth&) = delete;
    };
}


#endif /* _PAM_H_ */
