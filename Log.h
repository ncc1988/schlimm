/*
 *  This file is part of schlimm
 *  Copyright (C) 2019-2021  Moritz Strohm <ncc1988@posteo.de>
 *  and others (see the AUTHORS file).
 *
 *  SLiM - Simple Login Manager
 *  Copyright (C) 1997, 1998 Per Liden
 *  Copyright (C) 2004-06 Simone Rota <sip@varlock.com>
 *  Copyright (C) 2004-06 Johannes Winkelmann <jw@tks6.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 */


#ifndef SCHLIMM__LOG__H
#define SCHLIMM__LOG__H


#include <fstream>
#include <iostream>
#include <memory>


#include <fmt/core.h>


#include "const.h"


namespace Schlimm
{
    /**
     * This class handles logging of output.
     * It outputs everything that is passed via the log methods to stderr
     * and to a log file (if specified).
     */
    class Log
    {
        protected:


        /**
         * The SingletonEnforcer class is intentionally
         * left empty, because its only purpose is to
         * allow the creation of a Log class instance
         * as Singleton using std::make_shared which
         * requires the constructor to be public.
         */
        class SingletonEnforcer{};


        std::ofstream log_file;


        static std::shared_ptr<Log> instance;


        /**
         * The name of the application thas uses the Log class.
         * If app_name is non-empty, its value is prepended to a log message.
         */
        std::string app_name = "";


        public:


        Log(
            SingletonEnforcer s,
            const std::string& app_name = "",
            const std::string& log_file = ""
            );


        ~Log();


        static std::shared_ptr<Log> init(
            const std::string& app_name = "",
            const std::string& log_file = ""
            );


        static std::shared_ptr<Log> getInstance();


        /**
         * Appends a line with text to the log.
         */
        void append(const std::string& text);


        /**
         * Logs a text message.
         * This is practically the static version of the append method.
         */
        static void log(const std::string& text);
    };
}


#endif /* _LOG_H_ */
