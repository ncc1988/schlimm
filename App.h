/*
 *  This file is part of schlimm
 *  Copyright (C) 2019-2021  Moritz Strohm <ncc1988@posteo.de>
 *  and others (see the AUTHORS file).
 *
 *  SLiM - Simple Login Manager
 *  Copyright (C) 1997, 1998 Per Liden
 *  Copyright (C) 2004-06 Simone Rota <sip@varlock.com>
 *  Copyright (C) 2004-06 Johannes Winkelmann <jw@tks6.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef _APP_H_
#define _APP_H_


#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <vector>

#include <errno.h>
#include <fcntl.h>
#include <setjmp.h>
#include <signal.h>
#include <stdint.h>
#include <unistd.h>

#ifdef HAVE_SHADOW
#include <shadow.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

#include <X11/Xlib.h>
#include <X11/Xatom.h>

#include <fmt/core.h>

#include "Config.h"
#include "Image.h"
#include "NumLock.h"
#include "Panel.h"
#include "Util.h"
#include "DisplayClients/X11XLibClient.h"

#ifdef USE_PAM
#include <string>
#include "PAM.h"
#endif




/**
 * This is the "main" class of schlimm. It is a singleton.
 */
class App
{
    protected:


    /**
     * The SingletonEnforcer struct is intentionally protected and empty,
     * since its only purpose is to prevent calling the public constructor
     * for this singleton class.
     */
    struct SingletonEnforcer {};


    /**
     * The instance of this singleton class.
     */
    static std::shared_ptr<App> instance;


    /**
     * The instance of the Log class that is used
     * for logging messages.
     */
    std::shared_ptr<Schlimm::Log> log = nullptr;


    void login();


    void reboot();


    void halt();


    void suspend();


    void console();


    void Exit();


    void killAllClients(bool top);


    void readConfig();


    void hideCursor();


    void createServerAuth();


    void KillAllClients(Bool top);


    void ReadConfig();


    void HideCursor();


    void updatePID();


    bool authenticateUser(bool focuspass);


    static std::string findValidRandomTheme(const std::string &set);


    static void replaceVariables(
        std::string &input,
        const std::string &var,
        const std::string &value
        );


    /* Server functions */


    int startServer();


    /**
     * Waits until the X server process has changed its state.
     *
     * @param const uint16_t timeout The maximum amount of time
     *     (in seconds) this method should waid for the
     *     X server process to change its state.
     *
     * @param const std::string waiting_text The text to be logged
     *     during each loop where this method is waiting for the
     *     X server process to change its state.
     *
     * @returns bool True, if the X server process has changed its state.
     *     False is returned in case that the X server process
     *     either could not be found (no process with that PID)
     *     or the timeout for the X server process to change
     *     its state has been reached.
     */
    bool checkXServerProcess(const uint16_t timeout, const std::string& waiting_text);


    int waitForServer();


    [[deprecated]] void blankScreen();


    void setBackground(const std::string &themedir);


    [[deprecated]] Window root_window;


    /**
     * This holds the display client instance that is used
     * for graphical tasks.
     */
    std::shared_ptr<Schlimm::X11XLibClient> display_client = nullptr;


    /**
     *
     */
    [[deprecated]] Display* display;


    [[deprecated]] int screen;


    Schlimm::Panel* login_panel;


    /**
     *
     */
    pid_t server_pid;


    /**
     * The name of the used display.
     */
    std::string display_name;


    bool serverStarted;


#ifdef USE_PAM
    std::unique_ptr<PAM::PAMAuth> pam;
#endif


    /* Options */


    std::shared_ptr<Schlimm::Config> config;


    Pixmap BackgroundPixmap;


    Schlimm::Image* image;


    Atom BackgroundPixmapId;


    bool firstlogin;


    bool daemonmode;


    bool force_nodaemon;


    /* For testing themes */
    char *testtheme;


    bool testing;


    std::string themeName;


    std::string mcookie;


    const int mcookiesize = 32;  //Must be divisible by 4


    public:


    /**
     * Initialises the instance of this class.
     */
    static void init(int argc, char** argv);


    /**
     * The constructor for this singleton class. It is public so that
     * the instance of this class can be created with std::make_shared
     * which requires a public constructor. The protected SingletonEnforcer
     * struct makes sure that the constructor cannot be called from outside
     * the context of this class.
     */
    App(SingletonEnforcer s, int argc, char **argv);


    /**
     * Returns the instance of this class as std::shared_ptr.
     */
    static std::shared_ptr<App> getInstance();


    //~App();


    void run();


    /**
     * @returns The PID of the X server process.
     */
    int getServerPID();


    /**
     * Restarts the X server process.
     */
    void restartServer();


    /**
     * Stops the X server process.
     */
    void stopServer();


    /* Lock functions */

    void getLock();


    void removeLock();


    /**
     * Determines whether the X server process is running or not.
     *
     * @returns TODO
     */
    bool isServerStarted();


    static int handleXIOError(Display *disp);


    /**
     * Handles signals sent by the system.
     *
     * @param int sig The signal that shall be handled.
     */
    static void catchSignal(int sig);


    /**
     * Handles the USR1 signal.
     * This handler just reinstalls itself using signal()
     * and does nothing more.
     */
    static void handleUser1Signal(int sig);


    static int catchErrors(Display *dpy, XErrorEvent *ev);


    static int ignoreXIO(Display *d);
};


#endif
