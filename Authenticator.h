/*
 *  This file is part of schlimm
 *  Copyright (C) 2019  Moritz Strohm <ncc1988@posteo.de>
 *  and others (see the AUTHORS file).
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef AUTHENTICATOR_H
#define AUTHENTICATOR_H


#include "Panel.h"


namespace Schlimm
{
    /**
     * The Authenticator interface defines methods for authenticator
     * modules that handle the login process of users.
     */
    class Authenticator
    {
        public:


        /**
         * Implementations may use this method to setup
         * things that need to be done after construction
         * and before the session starts.
         */
        virtual void setup() = 0;


        /**
         * Gives authenticators access to the panel that displays
         * the GUI elements needed for the login screen.
         * Implementations must not take ownership of the panel!
         */
        virtual void setPanel(Panel* panel) = 0;


        /**
         * Starts an authentication session.
         */
        virtual void openSession() = 0;


        /**
         * Ends an authentication session.
         */
        virtual void closeSession() = 0;


        /**
         * Authenticates a user.
         */
        virtual void authenticate() = 0; //TODO: parameter definition (if necessary)
    };
}


#endif
